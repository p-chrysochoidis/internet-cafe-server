const collectionName = 'clientPcUsers';

exports.up = async (db) => {
    await db.createCollection(collectionName)
    return await db.addIndex(
            collectionName,
            'client-pc-user-username-unque-index',
            ['username'],
            true
        );
};

exports.down = (db) => {
    return db.dropCollection(collectionName);
};
