const collectionName = 'clientPc';

exports.up = async (db) => {
    await db.createCollection(collectionName)
    return await db.addIndex(
            collectionName,
            'client-pc-key-unque-index',
            ['key'],
            true
        );
};

exports.down = (db) => {
    return db.dropCollection(collectionName);
};
