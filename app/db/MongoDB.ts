import { MongoClient, Db, MongoClientOptions, Collection } from 'mongodb';
import { info } from 'loglevel';

import { Configuration } from '../utils/Configuration';
import { DBCollections } from './DBCollections';

class MongoDB {
    private mUri: string;
    private mConnectionOptions: MongoClientOptions;
    private mMongoClient: Promise<MongoClient>;

    constructor() {
        const {
            host,
            port,
            database,
            user,
            password,
            } = Configuration.MongoDb;
        // TODO: lock mongo and use user/password to connect
        const userEscaped = encodeURIComponent(user);
        const passwordEscaped = encodeURIComponent(password);
        const authMechanism = 'DEFAULT';
        this.mUri = `mongodb://${ host }:${ port }/${ database }/`;
        this.mConnectionOptions = {
            useNewUrlParser: true,
            poolSize: 30,
            socketTimeoutMS: 5000,
            connectTimeoutMS: 5000,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 3000,
            bufferMaxEntries: -1
        };
        this.connect();
    }

    private connect(): void {
        this.mMongoClient = new Promise<MongoClient>((res, rej) => {
            const client = new MongoClient(this.mUri, this.mConnectionOptions);
            const getConnection = () => client.connect()
                .then((c) => {
                    info('Mongodb connected');
                    res(c);
                })
                .catch(() => new Promise((res) => {
                    setTimeout(res, 1000);
                }).then(getConnection));
            getConnection();
        });
    }

    public async getDB(dbName: string = Configuration.MongoDb.database): Promise<Db> {
        return (await Promise.race([
            this.mMongoClient,
            new Promise<MongoClient>((_, rej) => setTimeout(rej, 5000)),
        ])).db(dbName);
    }

    public async getClientPCCollection(): Promise<Collection> {
        return (await this.getDB()).collection(DBCollections.clientPC)
    }
}
export const Mongo = new MongoDB();