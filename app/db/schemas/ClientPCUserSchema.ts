export interface ClientPCSchema {
    _id: string;
    type: string;
    username: string;
    password: string;
    session: string;
    loggedin: boolean;
    lastLoggedIn: string;
    lastLoggedOut: string;
    connectedClientPCKey: string;
    remainingPaidTime: number;
    remainingGiftTime: number;
    created: string;
    lastModified: string;
}
