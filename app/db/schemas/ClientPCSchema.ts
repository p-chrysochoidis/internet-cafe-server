export interface ClientPCSchema {
    _id: string;
    key: string;
    connected: boolean;
    lastConnected: string;
    ip: string;
    name: string;
    osArch: string;
    osRelease: string;
    osType: string;
    userName: string;
    token: string;
}