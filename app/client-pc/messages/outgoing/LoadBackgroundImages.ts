import { MessageModel } from '../MessageModel';

export interface LoadBackgroundImages extends MessageModel {
    images: string[];
}