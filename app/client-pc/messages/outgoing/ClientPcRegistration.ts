import { MessageModel } from '../MessageModel';

export interface ClientPcRegistration extends MessageModel {
    token: string;
}