export enum OutgoingMessageTypeEnum {
    loadBackgroundImages = 'loadBackgroundImages',
    clientPcRegisteration = 'clientPcRegisteration',
}