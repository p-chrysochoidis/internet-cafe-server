import { IncomingMessageTypeEnum } from './incoming/IncomingMessageTypeEnum';
import { OutgoingMessageTypeEnum } from './outgoing/OutgoingMessageTypeEnum';

export interface MessageModel {
    id?: string,
    type: IncomingMessageTypeEnum | OutgoingMessageTypeEnum,
}