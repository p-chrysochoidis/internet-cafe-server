export enum IncomingMessageTypeEnum {
    registerClientPc = 'registerClientPc',
    loadBackgroundImages = 'loadBackgroundImages',
}