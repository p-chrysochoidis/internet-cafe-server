import { MessageModel } from '../MessageModel';

export interface RegisterClientPcMessage extends MessageModel{
    key: string;
    pcIp: string;
    pcName: string;
    pcOsArch: string;
    pcOsRelease: string;
    pcOsType: string;
    pcUserName: string;
}