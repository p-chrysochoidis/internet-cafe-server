import * as io from 'socket.io';
import { BehaviorSubject, fromEvent, NEVER, Observable, of, race, from } from 'rxjs';
import { catchError, filter, map, share, take, tap, timeout, mergeMap } from 'rxjs/operators';
import { debug } from 'loglevel';

import { MessageModel } from './messages/MessageModel';
import { IncomingMessageTypeEnum } from './messages/incoming/IncomingMessageTypeEnum';
import { RegisterClientPcMessage } from './messages/incoming/RegisterClientPcMessage';
import { LoadBackgroundImages as LoadBackgroundImagesOut } from './messages/outgoing/LoadBackgroundImages';
import { OutgoingMessageTypeEnum } from './messages/outgoing/OutgoingMessageTypeEnum';
import { PC } from './PC';
import { ClientPcRegistration } from './messages/outgoing/ClientPcRegistration';

export class ClientPCConnection {
    private static readonly REGISTER_TIMEOUT_MILLIS = 5000;
    private readonly mSocket: io.Socket;
    private mMessagesStream: Observable<MessageModel>;
    private mPc: PC;
    public isConnected: BehaviorSubject<boolean> = new BehaviorSubject(true);

    private constructor(
        socket: io.Socket,
        messageStream: Observable<MessageModel>,
        pc: PC) {
        this.mSocket = socket;
        this.mMessagesStream = messageStream;
        this.mPc = pc;
        this.handleConnection();
        this.handleMessages();
    }

    public get key(): string {
        return this.mPc.key;
    }

    public static getFromInsecureSocket(notRegisteredPcSocket: io.Socket): Observable<ClientPCConnection | null> {
        const messagesStream = fromEvent<MessageModel>(notRegisteredPcSocket, 'message').pipe(share());
        let registerMessageId = null;
        return race(
            NEVER.pipe(timeout(ClientPCConnection.REGISTER_TIMEOUT_MILLIS)).pipe(
                catchError(() => {
                    debug(`[ClientPc] insecure socket (${ notRegisteredPcSocket.id }) forced to disconnect`);
                    notRegisteredPcSocket.disconnect(true);
                    return of(null);
                })
            ),
            messagesStream.pipe(
                filter((message: MessageModel) => message.type === IncomingMessageTypeEnum.registerClientPc),
                map((msg) => <RegisterClientPcMessage> msg),
                take(1),
                tap((message: RegisterClientPcMessage) => {
                   debug(`[ClientPc] register message received. Pc key ${message.key}`);
                   registerMessageId = message.id;
                }),
                mergeMap((message: RegisterClientPcMessage) => from(PC.fromRegisterMessage(message))),
                map((pc: PC) => {
                    const clientConnection = new ClientPCConnection(notRegisteredPcSocket, messagesStream, pc);
                    clientConnection.registrationReply(registerMessageId);
                    return clientConnection;
                }),
                catchError((error) => {
                    debug(`[ClientPc] insecure socket (${ notRegisteredPcSocket.id }) error creating PC, forced to disconnect`, error);
                    notRegisteredPcSocket.disconnect(true);
                    return of(null);
                })
            )
        );
    }

    private handleConnection() {
        this.mSocket.on('disconnect', () => {
            this.mPc.disconnect();
            debug(`Client pc ${ this.key } connection closed`);
            this.isConnected.next(false);
        });
    }

    private handleMessages() {
        this.mSocket.on('message', (msg: MessageModel) => {
            debug(`Received message from ${ this.key }`, msg);
            switch (msg.type) {
                case IncomingMessageTypeEnum.loadBackgroundImages:
                    const { id } = msg;
                    const imagesMsg: LoadBackgroundImagesOut = {
                        id,
                        type: OutgoingMessageTypeEnum.loadBackgroundImages,
                        images: ['https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?cs=srgb&dl=beautiful-beauty-blue-414612.jpg&fm=jpg'],
                    };
                    this.mSocket.emit('message', imagesMsg);
                    break;
            }
        });
    }

    private registrationReply(replyForMsgID: string) {
        const registrationMsg: ClientPcRegistration = {
            id: replyForMsgID,
            type: OutgoingMessageTypeEnum.clientPcRegisteration,
            token: this.mPc.token,
        }
        this.mSocket.emit('message', registrationMsg);
    }
}