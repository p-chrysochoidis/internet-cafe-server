import { Observable, Subscription } from 'rxjs';
import * as io from 'socket.io';
import { filter, finalize, mergeMap, take, tap } from 'rxjs/operators';
import { debug } from 'loglevel';

import { ClientPCConnection } from './ClientPCConnection';

export class ClientPcManager {
    private readonly mConnectionsStream: Observable<io.Socket>;
    private mConnectionsStreamSub: Subscription;
    private readonly mClientsByKey: Map<string, ClientPCConnection> = new Map();

    constructor(connectionsStream) {
        this.mConnectionsStream = connectionsStream;
        this.handleNewConnections();
    }

    private handleNewConnections() {
        this.mConnectionsStreamSub = this.mConnectionsStream.pipe(
            tap((socket) => debug('new connection received', socket.id)),
            mergeMap((socket) => ClientPCConnection.getFromInsecureSocket(socket)),
            filter((client) => client !== null),
        )
            .subscribe(
                (client) => {
                    this.mClientsByKey.set(client.key, client);
                    debug(`new client pc ${client.key} ready. Total clients ${this.mClientsByKey.size}`);
                    this.handleClientDisconnection(client);
                },
                (error) => {
                    debug('error with stream', error);
                },
            );
    }

    private handleClientDisconnection(client) {
        client.isConnected.pipe(
            filter((connected) => !connected),
            take(1),
            finalize(() => debug(`Client ${client.key} disconnected and removed from manager`)),
        )
            .subscribe((connected) => {
                    this.mClientsByKey.delete(client.key);
                    debug(`Client disconnected ${client.key}. Connected: ${connected}. Total clients: ${this.mClientsByKey.size}`);
                },
            );
    }
}