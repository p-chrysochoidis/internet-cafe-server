import { Collection } from "mongodb";

import { RegisterClientPcMessage } from "./messages/incoming/RegisterClientPcMessage";
import { Mongo } from "../db/MongoDB";
import { ClientPCSchema } from "../db/schemas/ClientPCSchema";
import { TokensGenerator } from "../utils/TokensGenerator";

export class PC {
    private mID: string;
    private mKey: string;
    private mConnected: boolean;
    private mLastConnected: Date;
    private mIp: string;
    private mName: string;
    private mOsArch: string;
    private mOsRelease: string;
    private mOsType: string;
    private mUserName: string;
    private mToken: string;

    constructor(values: ClientPCSchema) {
        const {
            _id = null,
            key = null,
            connected = null,
            lastConnected = null,
            ip = null,
            name = null,
            osArch = null,
            osRelease = null,
            osType = null,
            userName = null,
            token = null,
        } = values;
        this.mID = _id;
        this.mKey = key;
        this.mConnected = connected;
        this.mLastConnected = lastConnected && new Date(lastConnected) || null;
        this.mIp = ip;
        this.mName = name;
        this.mOsArch = osArch;
        this.mOsRelease = osRelease;
        this.mOsType = osType;
        this.mUserName = userName;
        this.mToken = token;
    }

    static async fromRegisterMessage(message: RegisterClientPcMessage): Promise<PC> {
        const clientPCs: Collection<ClientPCSchema> = await Mongo.getClientPCCollection();
        const notConnectedPcResult = await clientPCs.findOneAndUpdate(
            {
                key: message.key,
                connected: false,
            },
            {
                $set: {
                    key: message.key,
                    connected: true,
                    lastConnected: new Date().toISOString(),// TODO: mongo date?
                    ip: message.pcIp,
                    name: message.pcName,
                    osArch: message.pcOsArch,
                    osRelease: message.pcOsRelease,
                    osType: message.pcOsType,
                    userName: message.pcUserName,
                    token: TokensGenerator.getClientPcToken(),
                },
            },
            {
                upsert: true,
                returnOriginal: false,
            }
        );
        const { lastErrorObject: { n }, value } = notConnectedPcResult;
        if (!n) {
            throw new Error('Pc already connected');// TODO: better errors
        }
        return new PC(value);
    }

    public get key (): string {
        return this.mKey;
    }

    public get token (): string {
        return this.mToken;
    }

    public async disconnect() {
        // Todo log to db
        this.mConnected = false;
        if (this.mID) {
            const clientPCs: Collection<ClientPCSchema> = await Mongo.getClientPCCollection();
            await clientPCs.findOneAndUpdate(
                { _id: this.mID },
                { $set: {
                    connected: false,
                    token: null,
                } },
            )
        }
    }
}
