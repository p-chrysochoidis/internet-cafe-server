import { createHash } from 'crypto';

import { Configuration } from './Configuration';

const clientPCSecret = Configuration.TokensSecrets.clientPC;

export class TokensGenerator {
    public static getClientPcToken() {
        return TokensGenerator.sha256(`${ clientPCSecret }${ Date.now() }${ clientPCSecret }`);
    }

    private static sha256(input: string) {
        return createHash('sha256').update(input).digest('hex');
    }
}