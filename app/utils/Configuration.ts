export class Configuration {
    public static MongoDb = {
        host: 'mongo',
        port: 27017,
        database: 'ic_server',
        user: process.env.MONGO_USER || '',
        password: process.env.MONGO_PASS || '',
    };
    public static WebServer = {
        host: '0.0.0.0',
        port: 8080,
    };
    public static SocketServer = {
        host: '0.0.0.0',
        port: 8081,
        path: '/stream',
        serveClient: false,
        pingTimeout: 1000,
        pingInterval: 5000,
        transports: ['websocket'],
        cookie: false,
    };
    public static TokensSecrets = {
        clientPC: process.env.CLIENT_PC_TOKEN_SECRET || '3340bccd841bab852f8ab4264d8710bfce71e7f945adbf88f40769e6f584cc41',
    }
}
