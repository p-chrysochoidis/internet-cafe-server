import * as express from 'express';

import { pcClientRouter } from './pc-client/pc-client-router';
import { Mongo } from '../../db/MongoDB';

export const apiApp: express.Application = express();

apiApp.use(express.json());

apiApp.use('/pc-client', pcClientRouter);

apiApp.route('/test')
    .get(async (_, req: express.Response) => {
        try {
            const db = await Mongo.getDB('testing');
            const cursor = db.collection('testCol')
                .find({})
                .limit(100);
            req.json(await cursor.toArray());
            cursor.close();
        } catch(e) {
            console.log(e);
            req.sendStatus(500);
        }
    })
    .post(async (_, req: express.Response) => {
        try {
            const db = await Mongo.getDB('testing');
            const name = `${ Math.random() }`;
            const result = await db.collection('testCol').insertOne({
                name,
            });
            req.send(`added ${ name } ${ JSON.stringify(result) }`);
        } catch(e) {
            req.sendStatus(500);
        }
    });
