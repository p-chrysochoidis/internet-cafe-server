import * as express from 'express';

import { sessionRouter } from './session';

export const userRouter: express.Router = express.Router();

userRouter.use('/session', sessionRouter);
