import { Router, Request, Response } from "express";

export const sessionRouter: Router = Router();

sessionRouter.post('/', (req: Request, res: Response) => {
    res.send(`login ${ JSON.stringify(req.body, null, 4) }`);
});

sessionRouter.delete('/', (req: Request, res: Response) => {
    res.send(`delete ${ JSON.stringify(req.headers, null, 4) }`);
});
