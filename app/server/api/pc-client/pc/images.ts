import { Router, Request, Response } from "express";

export const imagesRouter: Router = Router();

imagesRouter.get('/', (req: Request, res: Response) => {
    res.send('images');
});

imagesRouter.get('/:imageID', (req: Request, res: Response) => {
    const { imageID } = req.params; 
    res.send(`serve image ${ imageID }`);
});
