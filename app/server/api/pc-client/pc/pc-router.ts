import * as express from 'express';

import { imagesRouter } from './images';

export const pcRouter: express.Router = express.Router();

pcRouter.use('/images', imagesRouter);
