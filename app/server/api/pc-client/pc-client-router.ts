import * as express from 'express';

import { pcRouter } from './pc/pc-router';
import { userRouter } from './user/user-router';

export const pcClientRouter: express.Router = express.Router();

pcClientRouter.use('/pc', pcRouter);
pcClientRouter.use('/user', userRouter);
