import * as express from 'express';
import { info } from 'loglevel';
import { Server } from 'http';

import { Configuration } from '../utils/Configuration';
import { Mongo } from '../db/MongoDB';
import { apiApp } from './api/api-app';

export class WebApiServer {
    private mApp: express.Application;
    private mHost: string;
    private mPort: number;
    private mHttpServer: Server;

    constructor() {
        this.mApp =  apiApp;
        this.mHost = Configuration.WebServer.host;
        this.mPort = Configuration.WebServer.port;
    }

    public start() {
        this.mHttpServer = this.mApp.listen(this.mPort, this.mHost, () => {
            info(`Api server started. Listening ${ this.mHost }/:${ this.mPort}`);
        });
    }

    public stop() {
        info('Api server shutting down');
        if (this.mHttpServer) {
            this.mHttpServer.close((error) => info('Api server stopped.', error || 'no error'));
        }
        this.mHttpServer = null;
    }
}