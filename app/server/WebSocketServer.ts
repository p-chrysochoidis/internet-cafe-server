import * as http from 'http';
import * as io from 'socket.io';
import { debug, info } from 'loglevel';
import { fromEvent, Observable } from 'rxjs';
import { share, tap } from 'rxjs/operators';

import { Configuration } from '../utils/Configuration';

export class WebSocketServer {
    private readonly mHttpServer: http.Server;
    private readonly mSocketServer: io.Server;
    private readonly mHost: string;
    private readonly mPort: number;
    private mConnectionsEmitter: Observable<io.Socket>;

    constructor() {
        const {
            host,
            port,
            path,
            serveClient,
            pingTimeout,
            pingInterval,
            transports,
            cookie,
        } = Configuration.SocketServer;
        this.mHost = host;
        this.mPort = port;
        this.mHttpServer = new http.Server();
        this.mSocketServer = io(this.mHttpServer, {
            path,
            serveClient,
            pingTimeout,
            pingInterval,
            transports,
            cookie,
        });
    }

    public connect(): Observable<io.Socket> {
        if (this.mConnectionsEmitter) {
            debug('Websocket server already initialized');
            return this.mConnectionsEmitter;
        }
        this.mConnectionsEmitter = fromEvent<io.Socket>(<any>this.mSocketServer, 'connect').pipe(
            tap((socket: io.Socket) => debug('New connection', socket.id)),
            share()
        );
        this.mHttpServer.listen(this.mPort, this.mHost, () => {
            info(`Websocket Http server listening ${this.mHost}:${this.mPort}`);
        });
        return this.mConnectionsEmitter;
    }

    public close() {
        info(`Closing websocket`);
        this.mHttpServer.close(() => info('WebSocket Http server closed'));
        this.mSocketServer.close(() => info('Websocket closed'));
    }
}