import { info, setDefaultLevel } from 'loglevel';

import { WebSocketServer } from './server/WebSocketServer';
import { ClientPcManager } from './client-pc/ClientPcManager';
import { WebApiServer } from './server/WebApiServer';

setDefaultLevel((process.env.NODE_ENV === 'development') ? 'trace' : 'info');

info('Starting internet cafe server...');
const webApiServer = new WebApiServer();
webApiServer.start();
const webSocketServer: WebSocketServer = new WebSocketServer();
const clientPcManager: ClientPcManager = new ClientPcManager(webSocketServer.connect());