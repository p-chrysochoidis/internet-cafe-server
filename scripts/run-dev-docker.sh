#!/bin/bash

BASEDIR=$(dirname "$0")
ROOT=$(cd $BASEDIR && cd ../ && pwd)
IMAGE_NAME="internet-cafe-server-development"

docker run -it --rm -v $ROOT:/app --user $(id -u) $IMAGE_NAME
